<?php

namespace App;

use Symfony\Component\DomCrawler\Crawler;
use GuzzleHttp\Client;

class AvitoParser
{
    const AVITO_DOMAIN = 'avito.ru';
    const DELAY_BEFORE_NEXT_REQUEST = 3;
    const ITEMS_PER_PAGE = 50;

    private $url;
    private $final_page;
    private $dbh;
    private $page_data = [];

    public function __construct(string $url, int $final_page = 0)
    {
        $this->url = $url;
        $this->final_page = $final_page ? ($final_page + 1) : 0;
        $this->dbh = DB::getDbh();
    }

    /**
     * Parsing data start
     */
    public function parsingStart(): void
    {
        echo "Parsing has started!\n";

        $guzzle = new Client();
        $current_page = 1;

        do {
            echo "Start page: {$current_page}\n";

            $url = $this->url . "&s=104&p={$current_page}";
            $response = $guzzle->request('GET', $url);

            $code = $response->getStatusCode();
            $content = $response->getBody()->getContents();

            if ($code !== 200) {
                exit("Error: response code {$code}\n");
            }

            $crawler = new Crawler($content);
            $items = $crawler->filterXPath('//div[contains(@class, "js-catalog-item-enum")]');

            $this->page_data = [];

            foreach ($items as $item) {
                $current_item = $this->parseSingleItem($item);

                if (!$current_item) {
                    continue;
                }

                $this->page_data[] = $current_item;
            }

            $current_page++;
            $items_count = count($items);

            $this->pushDataToDb($this->page_data);

            sleep(self::DELAY_BEFORE_NEXT_REQUEST);
        } while ($items_count < self::ITEMS_PER_PAGE || ($this->final_page > 0 && $current_page < $this->final_page));
    }

    /**
     * Returns data of single item
     *
     * @param  $item
     * @return array|null
     */
    private function parseSingleItem($item): ?array
    {
        $item = new Crawler($item);
        $item_data = [];

        $item_data['item_link'] = self::AVITO_DOMAIN . $item->filterXPath('//a[contains(@class, "item-description-title-link")]')->attr('href');
        echo "Start item: {$item_data['item_link']}\n";

        if ($this->checkItemInDb($item_data['item_link'])) {
            echo "Item with that link already exists\n";

            try {
                $elem = $item->filterXPath('//i[contains(@class, "monetization-icon_xs")]')->nodeName();

                if ($elem === 'i') {
                    echo "This is paid item, go to next..\n";
                    return null;
                }
            } catch (\Exception $e) {
                // It's item has no paid icon
            }

            if (count($this->page_data)) {
                $this->pushDataToDb($this->page_data);
            }

            exit("Parsing finished!\n");
        }

        $item_data['item_title'] = trim($item->filterXPath('//h3[contains(@class, "item-description-title")]')->text());
        $item_data['item_price'] = trim($item->filterXPath('//div[contains(@class, "about")]')->text());
        $item_data['item_photo'] = $this->getItemPhoto($item);
        $item_data['item_description'] = $this->getItemDescription($item);
        $item_data['company_link'] = $this->getCompanyLink($item);

        $company_scope = $this->getCompanyScope($item);
        $item_data['company_scope'] = $company_scope[0];

        $item_data['company_type'] = isset($company_scope[1]) ? $company_scope[1] : null;
        $item_data['company_region'] = $this->getCompanyRegion($item);

        $strdate = trim($item->filterXPath('//div[contains(@class, "c-2")]')->text());
        $item_data['item_created_at'] = DateReverser::getTimestamp($strdate);

        return $item_data;
    }

    /**
     * Gets URL photo of item
     *
     * @param  Crawler $item
     * @return null|string
     */
    private function getItemPhoto(Crawler $item): ?string
    {
        // TODO: to make debug that method
        $photo = null;

        try {
            $photo = $item->filterXPath('//img[contains(@class, "photo-count-show")]')->attr('src');
            $photo = preg_replace('/^\/{2}/', '', $photo);
        } catch (\Exception $e) {
            // Item has no a photo, do nothing
        }

        if (!$photo) {
            try {
                $photo = $item->filterXPath('//div[contains(@class, "item-slider-image")]')->attr('style');
                $photo = preg_replace('/^background-image: url\(\/\/|\)$/', '', $photo);
            } catch (\Exception $e) {
                // Item has no the slide photos, do nothing
            }
        }

        return $photo;
    }

    /**
     * Gets item description
     *
     * @param  Crawler $item
     * @return null|string
     */
    private function getItemDescription(Crawler $item): ?string
    {
        $description = null;

        try {
            $description = trim($item->filterXPath('//a[contains(@class, "js-item_table-extended-description")]')->text());
        } catch (\Exception $e) {
            // Item has no description, do nothing
        }

        return $description;
    }

    /**
     * Gets company link
     *
     * @param  Crawler $item
     * @return null|string
     */
    private function getCompanyLink(Crawler $item): ?string
    {
        $company_link = null;

        try {
            $company_link = $item->filterXPath('//div[contains(@class, "data")]/p/a')->attr('href');
        } catch (\Exception $e) {
            // Company has no a link, do nothing
        }

        if (!$company_link) {
            try {
                $company_link = $item->filterXPath('//div[contains(@class, "data")]/a')->attr('href');
            } catch (\Exception $e) {
                // Company has no a verified link, do nothing
            }
        }

        if ($company_link) {
            $company_link = self::AVITO_DOMAIN . $company_link;
        }

        return $company_link;
    }

    /**
     * Gets company info
     *
     * @param  Crawler $item
     * @return array
     */
    private function getCompanyScope(Crawler $item): array
    {
        $company_scope = $item->filterXPath('//div[contains(@class, "data")]/p')->first()->text();
        $company_scope = explode('|', $company_scope);
        $company_scope = array_map('trim', $company_scope);

        return $company_scope;
    }

    /**
     * Gets company region
     *
     * @param  Crawler $item
     * @return null|string
     */
    private function getCompanyRegion(Crawler $item): ?string
    {
        // TODO: to make debug that method
        $region = trim($item->filterXPath('//div[contains(@class, "data")]/p')->nextAll()->text());

        if (preg_match('/\d{2}:\d{2}/', $region)) {
            // It's a date
            $region = null;
        }

        return $region;
    }

    /**
     * Checks the item are in DB
     *
     * @param  string $item_link
     * @return null|array
     */
    private function checkItemInDb(string $item_link): ?array
    {
        $sth = $this->dbh->prepare('SELECT * FROM avito WHERE item_link = :item_link');
        $sth->execute(['item_link' => $item_link]);

        return $sth->fetch() ?: null;
    }

    /**
     * Pushes data to DB
     *
     * @param array $page_data
     */
    private function pushDataToDb(array $page_data): void
    {
        echo "Start push data to DB\n";

        $columns_count = count($page_data[0]);
        $items_count = count($page_data);
        $length = $items_count * $columns_count;

        // Prepare substring for query args
        $args = implode(',', array_map(
            function ($item) {
                return '(' . implode(',', $item) . ')';
            }, array_chunk(array_fill(0, $length, '?'), $columns_count)
        ));

        $params = [];

        // Collect args to one array
        /** @var array $page_data */
        foreach ($page_data as $row) {
            /** @var array $row */
            foreach ($row as $value) {
                $params[] = $value;
            }
        }

        $sql = 'INSERT INTO avito (
            item_link, item_title, item_price, item_photo, item_description, company_link, company_scope, company_type, company_region, item_created_at
        ) VALUES ' . $args;

        $sth = $this->dbh->prepare($sql);
        $sth->execute($params);
    }
}