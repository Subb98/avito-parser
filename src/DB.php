<?php

namespace App;

class DB {
    private static $dbh;

    private function __construct() {}
    private function __clone () {}

    /**
     * Get the Database handle instance
     * 
     * @return \PDO
     */
    public static function getDbh(): \PDO
    {
        if(!self::$dbh) {
            try {
                $dsn = "mysql:dbname={$_ENV['DB_DATABASE']};host={$_ENV['DB_HOST']}";
                self::$dbh = new \PDO($dsn, $_ENV['DB_USER'], $_ENV['DB_PASSWORD']);
                self::$dbh->exec('SET NAMES utf8');
                self::$dbh->exec('SET CHARACTER SET utf8');
            } catch(\PDOException $error) {
                echo $error->getMessage();
            }
        }

        return self::$dbh;
    }
}