<?php

namespace App;

class DateReverser
{
    const DATES = [
        'now' => 'сегодня',
        '-1 day' => 'вчера',
    ];
    
    const MONTHS = [
        'Jan' => 'января',
        'Feb' => 'февраля',
        'Mar' => 'марта',
        'Apr' => 'апреля',
        'May' => 'мая',
        'Jun' => 'июня',
        'Jul' => 'июля',
        'Aug' => 'августа',
        'Sep' => 'сентября',
        'Oct' => 'октября',
        'Nov' => 'ноября',
        'Dec' => 'декабря',
    ];

    private function __construct() {}
    private function __clone () {}

    /**
     * Get timestamp from datestring
     * 
     * @param  string  $input
     * @return string
     */
    public static function getTimestamp(string $input): string
    {
        $sort_date = false;
        $array = mb_split('\s', $input);

        if (count($array) > 2) {
            $dictionary = self::MONTHS;
        } else {
            array_unshift($array, null);
            $dictionary = self::DATES;
            $sort_date = true;
        }

        $substr = mb_strtolower($array[1]);

        foreach ($dictionary as $key => $value) {
            if ($substr === $value) {
                $array[1] = $key;
                break;
            }
        }

        $strdate = trim(implode(' ', $array));

        if ($sort_date) {
            $date = new \DateTime($strdate);
        } else {
            $date = \DateTime::createFromFormat('j M H:i', $strdate);
        }

        return $date->format('Y-m-d H:i:s');
    }
}