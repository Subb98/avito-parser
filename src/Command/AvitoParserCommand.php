<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use App\AvitoParser;

class AvitoParserCommand extends Command
{
    protected function configure()
    {
        // Example: ./bin/console avito:start "https://www.avito.ru/perm?q=доска" 2
        $this->setName('avito:start')
             ->setDescription('Start Avito parser')
             ->addArgument('url', InputArgument::REQUIRED, 'URL for parsing')
             ->addArgument('final_page', InputArgument::OPTIONAL, 'Final page num for parsing', 3);
    }

    protected function execute (InputInterface $input, OutputInterface $output)
    {
        $url = $input->getArgument('url');
        $final_page = $input->getArgument('final_page');

        $parser = new AvitoParser($url, $final_page);
        $parser->parsingStart();
    }
}