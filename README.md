# Avito parser

## Usage syntax:
```avito:start <url> [final_page]```

- note: if the final_page param is not defined, then the parsing will be up to the last page
- note 2: items will be sorted by date

## Usage example:
```./bin/console avito:start "https://www.avito.ru/perm?q=доска" 2```